using System;
using System.Collections.Generic;

namespace coin_flip
{
    public class flipSimulator
    {
        public flipSimulator()
        {
        }

        private List<String> coinList = new List<String>();
        private Random rnd = new Random();

        public void flips(int n)
        {
            for (int i = 0; i < n; i++)
            {
                if (rnd.Next(0, 2) == 1)
                {
                    coinList.Add("Heads");
                } else
                {
                    coinList.Add("Tails");
                }
            }
            coinList.ForEach(Console.WriteLine);
        }
    }
}

